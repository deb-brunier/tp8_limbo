# A TRIBUTE TO "LIMBO"
<img width="20%" src="./assets/img/icone.png">


## TECHNO-TAGS
* HTML5
* CSS3
* SASS/ BEM
* Bootstrap
* JS
* Parallax

## AUTEURE
* Déb Phoenix ~ *good vibes only*
* Proudly students powered by: [Simplon.co](http://simplon.co)

## Copyright
* [Limbo](https://playdead.com/games/limbo/)